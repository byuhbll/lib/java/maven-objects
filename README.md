# Maven Parents and Archetypes

Apache Maven is a build and management tool for Java projects. All BYU Library Java projects, including libraries, executables, and applications, are built and managed using Maven. This repository provides the parent POMs (common build configuration and dependencies) and archetypes (project templates) for such Java projects.

This repository is organized as a multi-module maven project with the following components:

1. An overall parent POM to be inherited by all Java projects.
2. A library parent POM and archetype to be used by all Java libraries.
3. An executable parent POM and archetype to be used by all Java executables such as CLIs and cron jobs.
4. An application parent POM and archetype to be used by all Java web applications.

Note: All instructions on developing and using this project assume Docker and VS Code equipped with the Dev Containers extension are installed. Though Docker and VS Code are not required, instructions are not provided for alternative methods.

## Developing This Project

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

First, update the project version. Do this by running `mvn versions:set -DgenerateBackupPoms=false -DnewVersion=X.X.X` from the container command line. This will change all the versions in the POMs to `X.X.X`. Also perform a search and replace for the version across all files in order to catch the remaining versions listed in the readmes.

To build and install the project for local testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project to Maven Central will make these parent POMs and archetypes easily accessible via the Maven tool. The configured GitLab pipeline will automatically build the project, but deploying to Maven Central requires manually playing the deploy job.

## Creating a Java Project

Creating a new Java project is done by running one of the commands below. If testing a development version of this project, you will first need to build and install the project according to the instructions in Developing This Project and then modify the archetype version in the command: `-DarchetypeVersion=5.2.1`.

### Library

Execute the following from the command line to create a new library project. You will be prompted to provide information about your project. See the [Library Archetype](library-archetype/) for more information.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn archetype:generate -Duser.home=/var/maven -DoutputDirectory=/project -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=library-archetype -DarchetypeVersion=5.2.1
```

### Executable

Execute the following from the command line to create a new executable project. You will be prompted to provide information about your project. See the [Executable Archetype](executable-archetype/) for more information.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn archetype:generate -Duser.home=/var/maven -DoutputDirectory=/project -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=executable-archetype -DarchetypeVersion=5.2.1
```

### Application

Execute the following from the command line to create a new application project. You will be prompted to provide information about your project. See the [Application Archetype](application-archetype/) for more information.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn archetype:generate -Duser.home=/var/maven -DoutputDirectory=/project -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=application-archetype -DarchetypeVersion=5.2.1
```

## Java Version

The main parent POM specifies the Java version for all child poms. To override the java version in a child pom, add this to your pom.xml.

```xml
<project>
  <properties>
    <java.version>17</java.version>
  </properties>
</project>
```
