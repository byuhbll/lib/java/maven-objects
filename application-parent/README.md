# Application Parent POM #

This is the parent pom for all BYU HBLL web applications. This is based on Spring Boot.

This POM can be referenced by adding the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>application-parent</artifactId>
  <version>5.2.1</version>
</parent>
```

## Keycloak

Spring Security with Keycloak support is included in the parent pom and enabled by default. To configure or disable it, visit the [Spring Keycloak project page](https://gitlab.com/byuhbll/lib/java/spring-keycloak) for configuration options.
