<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>parent</artifactId>
  <version>5.2.1</version>
  <name>Java Parent POM</name>
  <description>Parent POM for all Java projects</description>
  <packaging>pom</packaging>

  <url>https://gitlab.com/byuhbll/lib/java/maven</url>

  <licenses>
    <license>
      <name>3-Clause BSD License</name>
      <url>https://opensource.org/licenses/BSD-3-Clause</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <developers>
    <developer>
      <name>Harold B. Lee Library</name>
      <email>hblldev@byu.edu</email>
      <organization>Harold B. Lee Library, Brigham Young University</organization>
      <organizationUrl>https://developer.lib.byu.edu/</organizationUrl>
    </developer>
  </developers>

  <scm>
    <connection>scm:git:git://gitlab.com/byuhbll/lib/java/maven.git</connection>
    <developerConnection>scm:git:ssh://gitlab.com:byuhbll/lib/java/maven.git</developerConnection>
    <url>https://gitlab.com/byuhbll/lib/java/maven</url>
  </scm>

  <distributionManagement>
    <repository>
      <id>${repositoryId}</id>
      <url>${repositoryUrl}</url>
    </repository>
  </distributionManagement>

  <properties>
    <java.version>21</java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <jackson.version>2.18.2</jackson.version>
    <kafka.version>3.9.0</kafka.version>
    <junit.version>5.11.4</junit.version>
    <mockito.version>5.15.2</mockito.version>
    <testcontainers.version>1.20.4</testcontainers.version>
    <lombok.version>1.18.36</lombok.version>
    <hoverfly.version>0.19.1</hoverfly.version>
    <witness.version>1.3.0</witness.version>
    <misc.version>1.5.0</misc.version>
    <maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
    <jacoco-maven-plugin.version>0.8.12</jacoco-maven-plugin.version>
    <maven-surefire-plugin.version>3.5.2</maven-surefire-plugin.version>
    <maven-failsafe-plugin.version>3.5.2</maven-failsafe-plugin.version>
    <maven-checkstyle-plugin.version>3.6.0</maven-checkstyle-plugin.version>
    <puppycrawl.checkstyle.version>10.21.1</puppycrawl.checkstyle.version>
    <wagon-ssh.version>3.5.3</wagon-ssh.version>
    <nexus-staging-maven-plugin.version>1.7.0</nexus-staging-maven-plugin.version>
    <maven-gpg-plugin.version>3.2.7</maven-gpg-plugin.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.fasterxml.jackson</groupId>
        <artifactId>jackson-bom</artifactId>
        <version>${jackson.version}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>org.apache.kafka</groupId>
        <artifactId>kafka-clients</artifactId>
        <version>${kafka.version}</version>
      </dependency>
      <dependency>
        <groupId>org.junit</groupId>
        <artifactId>junit-bom</artifactId>
        <version>${junit.version}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>org.testcontainers</groupId>
        <artifactId>testcontainers-bom</artifactId>
        <version>${testcontainers.version}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>edu.byu.hbll.witness</groupId>
        <artifactId>witness-bom</artifactId>
        <version>${witness.version}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-bom</artifactId>
        <version>${mockito.version}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>edu.byu.hbll</groupId>
        <artifactId>misc</artifactId>
        <version>${misc.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-params</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-junit-jupiter</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${lombok.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.testcontainers</groupId>
      <artifactId>junit-jupiter</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.specto</groupId>
      <artifactId>hoverfly-java-junit5</artifactId>
      <version>${hoverfly.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>edu.byu.hbll.witness</groupId>
      <artifactId>witness-core</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven-compiler-plugin.version}</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>${jacoco-maven-plugin.version}</version>
        <executions>
          <execution>
            <id>pre-unit-test</id>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
            <configuration>
              <destFile>${project.build.directory}/coverage-reports/jacoco-ut.exec</destFile>
              <propertyName>surefireArgLine</propertyName>
            </configuration>
          </execution>
          <execution>
            <id>pre-integration-test</id>
            <goals>
              <goal>prepare-agent-integration</goal>
            </goals>
            <configuration>
              <destFile>${project.build.directory}/coverage-reports/jacoco-it.exec</destFile>
              <propertyName>failsafeArgLine</propertyName>
            </configuration>
          </execution>
          <execution>
            <id>post-integration-test</id>
            <phase>post-integration-test</phase>
            <goals>
              <goal>report</goal>
            </goals>
            <configuration>
              <dataFile>${project.build.directory}/coverage-reports/jacoco-it.exec</dataFile>
              <outputDirectory>${project.reporting.outputDirectory}/jacoco-it</outputDirectory>
            </configuration>
          </execution>
          <execution>
            <id>post-unit-test</id>
            <phase>prepare-package</phase>
            <goals>
              <goal>report</goal>
            </goals>
            <configuration>
              <dataFile>${project.build.directory}/coverage-reports/jacoco-ut.exec</dataFile>
              <outputDirectory>${project.reporting.outputDirectory}/jacoco-ut</outputDirectory>
            </configuration>
          </execution>
          <execution>
            <id>merge-results</id>
            <phase>verify</phase>
            <goals>
              <goal>merge</goal>
            </goals>
            <configuration>
              <fileSets>
                <fileSet>
                  <directory>${project.build.directory}/coverage-reports</directory>
                  <includes>
                    <include>*.exec</include>
                  </includes>
                </fileSet>
              </fileSets>
              <destFile>${project.build.directory}/coverage-reports/jacoco.exec</destFile>
            </configuration>
          </execution>
          <execution>
            <id>post-merge-report</id>
            <phase>verify</phase>
            <goals>
              <goal>report</goal>
            </goals>
            <configuration>
              <dataFile>${project.build.directory}/coverage-reports/jacoco.exec</dataFile>
              <outputDirectory>${project.reporting.outputDirectory}/jacoco</outputDirectory>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${maven-surefire-plugin.version}</version>
        <configuration>
          <argLine>${surefireArgLine}</argLine>
          <skipTests>${skip.unit.tests}</skipTests>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>${maven-failsafe-plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>integration-test</goal>
              <goal>verify</goal>
            </goals>
            <configuration>
              <argLine>${failsafeArgLine}</argLine>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>${maven-checkstyle-plugin.version}</version>
        <configuration>
          <configLocation>google_checks.xml</configLocation>
          <consoleOutput>true</consoleOutput>
          <violationSeverity>warning</violationSeverity>
        </configuration>
        <executions>
          <execution>
            <id>verify-checkstyle</id>
            <phase>verify</phase>
            <configuration>
              <violationSeverity>error</violationSeverity>
            </configuration>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
        <dependencies>
          <dependency>
            <groupId>com.puppycrawl.tools</groupId>
            <artifactId>checkstyle</artifactId>
            <version>${puppycrawl.checkstyle.version}</version>
          </dependency>
        </dependencies>
      </plugin>
    </plugins>
    <extensions>
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-ssh</artifactId>
        <version>${wagon-ssh.version}</version>
      </extension>
    </extensions>
  </build>

  <profiles>
    <profile>
      <id>central</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.sonatype.plugins</groupId>
            <artifactId>nexus-staging-maven-plugin</artifactId>
            <version>${nexus-staging-maven-plugin.version}</version>
            <extensions>true</extensions>
            <configuration>
              <serverId>ossrh</serverId>
              <nexusUrl>https://oss.sonatype.org/</nexusUrl>
              <autoReleaseAfterClose>true</autoReleaseAfterClose>
            </configuration>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-gpg-plugin</artifactId>
            <version>${maven-gpg-plugin.version}</version>
            <executions>
              <execution>
                <id>sign-artifacts</id>
                <goals>
                  <goal>sign</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

  <modules>
    <module>library-parent</module>
    <module>executable-parent</module>
    <module>application-parent</module>
    <module>library-archetype</module>
    <module>executable-archetype</module>
    <module>application-archetype</module>
  </modules>

</project>