# This CI/CD configuration provides a standard pipeline for:
# * building and testing a java maven library
# * deploying the maven library to an internal maven repository and to maven central
#
# This CI/CD configuration is meant to be used in conjunction with the maven parent pom
# `edu.byu.hbll.maven:library-parent`.
#
# This pipeline expects the following environment variables:
#   INTERNAL_MAVEN_REPO_URL - The url of the internal maven repo to deploy to.
#   INTERNAL_MAVEN_REPO_SSH_KEY - The ssh key to use for deploying to an internal maven repo. Should be base64 encoded.
#   SONATYPE_USERNAME - The sonatype username for deploying to maven central.
#   MAVEN_ENCRYPTED_MASTER_PASSWORD - The maven master password used for encrypting the sonatype password and PGP passphrase.
#   ENCRYPTED_SONATYPE_PASSWORD - The sonatype password encrypted by maven.
#   ENCRYPTED_PGP_PASSPHRASE - The pgp passphrase encrypted by maven.
#   MAVEN_CENTRAL_PGP_PRIVATE_KEY - The private pgp key used for signing maven artifacts. Should be base64 encoded.
#
# All of these environment variables should be defined at the group level, but can be overridden at the project level, too.
include:
  - project: byuhbll/support/gitlab/ci
    ref: '1'
    file: java-lib-deploy.yml

image: maven:3-ibm-semeru-21-jammy

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.showDateTime=true"
  MAVEN_CLI_OPTS: "--no-transfer-progress"
  VERSION: 5.0.0

cache:
  paths:
    - .m2/repository

stages:
  - test
  - deploy

library_min:
  stage: test
  variables:
    archetypeArgs: "-Ddescription=test"
  script:
    - new_project library "$archetypeArgs"
    - mvn $MAVEN_CLI_OPTS install

library_max:
  stage: test
  variables:
    archetypeArgs: "-Ddescription=test -DgroupId=com.example -Dpackage=com.example.mytest"
  script:
    - new_project library "$archetypeArgs"
    - mvn $MAVEN_CLI_OPTS install
    - ls src/main/java/com/example/mytest

executable_min:
  stage: test
  variables:
    archetypeArgs: "-Ddescription=test"
  script:
    - new_project executable "$archetypeArgs"
    - mvn package
    - java -jar target/testproject.jar

executable_max:
  stage: test
  variables:
    archetypeArgs: "-Ddescription=test -DgroupId=com.example -Dpackage=com.example.mytest"
  script:
    - new_project executable "$archetypeArgs"
    - mvn package
    - java -jar target/testproject.jar

application_min:
  stage: test
  variables:
    archetypeArgs: "-Ddescription=test -DwithFrontEnd=n"
  script:
    - new_project application "$archetypeArgs"
    - mvn verify package

application_max:
  stage: test
  variables:
    archetypeArgs: "-Ddescription=test -DgroupId=com.example -Dpackage=com.example.mytest"
  script:
    - new_project application "$archetypeArgs"
    - mvn verify package

.bash_functions: &bash_functions |
  function new_project() {
    PROJECT_TYPE=$1
    PROJECT_ARGS=$2

    CODE=$(curl -w %{http_code} -o /dev/null "http://repo1.maven.org/maven2/edu/byu/hbll/maven/${PROJECT_TYPE}-archetype/${VERSION}/${PROJECT_TYPE}-archetype-${VERSION}.jar")

    if [[ CODE -eq 200 ]]; then
      echo "It looks like the version you're testing is already in maven central. To make sure you don't re-test the version in maven central, update this project to a new version"
      exit 1
    fi

    cd /tmp
    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=$PROJECT_TYPE-archetype -DarchetypeVersion=$VERSION -DinteractiveMode=false -DartifactId=testproject $PROJECT_ARGS
    cd testproject
  }

before_script:
  - mvn $MAVEN_CLI_OPTS install
  - *bash_functions
